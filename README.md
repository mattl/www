# Matt Lee

This is my personal website. All the content is CC-BY-SA 4.0 unless
otherwise specified. All of my code is AGPLv3+.

Based on Jekyll Bootstrap:

For original project's usage and documentation please see:
<http://jekyllbootstrap.com> under the
[MIT](http://opensource.org/licenses/MIT) license.
